package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.controllers.ELMC;

import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.dto.VersionesTaquigraficasAsuntos;
import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.services.ELServ.VersionesTaqAsuntosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;


@RestController
@RequestMapping(path = "/versionesTaq")
public class EVTaquigraficasAsuntosController {

    @Autowired
    private VersionesTaqAsuntosService _VersionesTaqAsuntosService;

    @CrossOrigin
    @GetMapping(path = "/asuntos")
    public Page<VersionesTaquigraficasAsuntos> getVTaquigraficasPorAsuntos(@RequestParam(name = "page") Integer page,
                                                                           @RequestParam(name = "size") Integer size,
                                                                           @RequestParam(required = false, name = "filtros") String filtros) throws ParseException {

        return _VersionesTaqAsuntosService.getVersionesTaquigraficasByPleno(page, size, filtros);
    }

    @CrossOrigin
    @GetMapping(path = "/tiposAsuntos")
    public List<String> getTiposAsuntos() throws ParseException {

        return _VersionesTaqAsuntosService.getTiposAsuntos();
    }

    @CrossOrigin
    @GetMapping(path = "/countAsuntos")
    public Integer getVTaquigraficasPorAsuntosCount(@RequestParam(required = false, name = "filtros") String filtros) throws ParseException {

        return _VersionesTaqAsuntosService.getVersionesTaquigraficasByPlenoCountAsuntosHighligh(filtros);
    }
}
