package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.services.MongoServ;

import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.domains.AnyMongoDomain.VTaqAsuntosExportDomain;
import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.repositorys.AnyMongo.VTaquigraficasAsuntosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class VTaqAsuntosService {

    @Autowired
    private VTaquigraficasAsuntosRepository _vTaquigraficasAsuntosRepository;

    public  Map<LocalDate, List<VTaqAsuntosExportDomain>> getAllAsuntos() {
        List<LocalDate> listFechas = new ArrayList<>();
        List<VTaqAsuntosExportDomain> listOriginal = new ArrayList<>();
                _vTaquigraficasAsuntosRepository.findAll()
                .stream()
                .forEach(xx -> {
                    VTaqAsuntosExportDomain v1 = new VTaqAsuntosExportDomain();
                    v1.setNumExpediente(xx.getNumExpediente());
                    v1.setTipoAsunto(xx.getTipoAsunto());
                    v1.setTemasFondo(xx.getTemasFondo());
                    v1.setTemasProcesales(xx.getTemasProcesales());
                    v1.setSesionesTodas(xx.getSesionesTodas());
                    listOriginal.add(v1);
                    xx.getSesionesTodas().stream().forEach(
                            fecha -> {
                                //System.out.println(fecha);
                                listFechas.add(LocalDate
                                        .of(Integer.parseInt(fecha.split("-")[2]),
                                                Integer.parseInt(fecha.split("-")[1]),
                                                Integer.parseInt(fecha.split("-")[0])));
                            });
                });
        List<LocalDate> listFechasDist = listFechas.stream().distinct().collect(Collectors.toList());
        listFechasDist = listFechasDist.stream()
                .sorted(Collections.reverseOrder())
                .collect(Collectors.toList());

        Map<LocalDate, List<VTaqAsuntosExportDomain>> mapAsuntos = new HashMap<>();

        listFechasDist.stream().forEach(xx -> {
            String fechaUlt = xx.toString().split("-")[2] + "-" + xx.toString().split("-")[1] + "-" + xx.toString().split("-")[0];

            List<VTaqAsuntosExportDomain> listOriginal2 =
                    listOriginal.stream()
                    .filter(cc -> cc.getSesionesTodas().contains(fechaUlt)).collect(Collectors.toList());
            mapAsuntos.put(xx, listOriginal2);
        });

        Map<LocalDate, List<VTaqAsuntosExportDomain>> mapAsuntos2 = mapAsuntos.entrySet().stream()
                .sorted(Map.Entry.<LocalDate,List<VTaqAsuntosExportDomain>>comparingByKey().reversed())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e1, LinkedHashMap::new));
        return mapAsuntos2;
    }
}
