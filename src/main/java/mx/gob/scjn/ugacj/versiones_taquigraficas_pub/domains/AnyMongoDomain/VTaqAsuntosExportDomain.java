package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.domains.AnyMongoDomain;

import java.util.List;

public class VTaqAsuntosExportDomain {

    private String tipoAsunto;
    private String numExpediente;
    private List<String> temasProcesales;
    private List<String> temasFondo;
    private List<String> sesionesTodas;

    public String getTipoAsunto() {
        return tipoAsunto;
    }

    public void setTipoAsunto(String tipoAsunto) {
        this.tipoAsunto = tipoAsunto;
    }

    public String getNumExpediente() {
        return numExpediente;
    }

    public void setNumExpediente(String numExpediente) {
        this.numExpediente = numExpediente;
    }

    public List<String> getTemasProcesales() {
        return temasProcesales;
    }

    public void setTemasProcesales(List<String> temasProcesales) {
        this.temasProcesales = temasProcesales;
    }

    public List<String> getTemasFondo() {
        return temasFondo;
    }

    public void setTemasFondo(List<String> temasFondo) {
        this.temasFondo = temasFondo;
    }

    public List<String> getSesionesTodas() {
        return sesionesTodas;
    }

    public void setSesionesTodas(List<String> sesionesTodas) {
        this.sesionesTodas = sesionesTodas;
    }

    @Override
    public String toString() {
        return "VTaqAsuntosExportDomain{" +
                "tipoAsunto='" + tipoAsunto + '\'' +
                ", numExpediente='" + numExpediente + '\'' +
                ", temasProcesales=" + temasProcesales +
                ", temasFondo=" + temasFondo +
                ", sesionesTodas=" + sesionesTodas +
                '}';
    }
}
