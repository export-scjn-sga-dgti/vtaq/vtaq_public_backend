package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.services.ELServ;


import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.domains.ELM.BDVersionTaquigraficaDomain;
import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.domains.ELM.BDVtaqAsuntoDomain;
import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.domains.ELM.DBAsuntoAbordadoDomain;
import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.dto.AsuntosTemasDTO;
import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.dto.VTaquigraficaDTO;
import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.dto.VersionesTaquigraficasAsuntos;
import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.repositorys.ELMR.BDVersionTaquigraficaRepository;
import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.repositorys.ELMR.BDVtaqAsuntoRepository;
import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.repositorys.ELMR.DBAsuntoAbordadoRepository;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.core.SearchHit;
import org.springframework.data.elasticsearch.core.SearchHits;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.data.elasticsearch.core.query.UpdateQuery;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Criteria;

import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

@Service
public class ConvertVTaquigraficasService {

    @Autowired
    private ElasticsearchRestTemplate _elasticsearchTemplate;

    @Autowired
    private BDVersionTaquigraficaRepository _dbVersionTaquigraficaRepository;

    @Autowired
    private BDVtaqAsuntoRepository _bdVtaqAsuntoRepository;

    @Autowired
    private DBAsuntoAbordadoRepository _bdAsuntoAbordadoRepository;

    @Autowired
    private MongoTemplate _mongoTemplate;

    String indice = "vtaquigraficas";

    //Actuaizacion masiva de las Versiones Taquigraficas de Elastic con la informacion SGA Mongo
    public List<VTaquigraficaDTO> coversionDatos(int page) {
        List<VTaquigraficaDTO> listVtaqAsuntos = new ArrayList<>();

        int size = 1000; // 800;
        Pageable pageable = PageRequest.of(page, size);
        QueryBuilder queryBuilder = QueryBuilders.boolQuery()
                .must(QueryBuilders.matchQuery("organoJurisdiccional", "Pleno").operator(Operator.AND))
                .must(QueryBuilders.rangeQuery("anio").gte(2019));
        Query searchQuery = new NativeSearchQueryBuilder()
                .withQuery(queryBuilder)
                .withPageable(pageable)
                .build();
        searchQuery.addSort(Sort.by(Sort.Direction.DESC, "fechaSesion"));
        SearchHits resultHits = _elasticsearchTemplate.search(searchQuery, VersionesTaquigraficasAsuntos.class, IndexCoordinates.of(indice));
        AtomicInteger count = new AtomicInteger(0);
        resultHits.getSearchHits().forEach(xx -> {
            SearchHit result = (SearchHit) xx;
            VersionesTaquigraficasAsuntos vtaq = (VersionesTaquigraficasAsuntos) result.getContent();
            String fecha = vtaq.getFechaSesion().toString();
            fecha = fecha.split("/")[2] + "-" + fecha.split("/")[1] + "-" + fecha.split("/")[0];
            System.out.println(count.incrementAndGet() + ".- " + vtaq.getId() + " " + fecha + " <-> " + vtaq.getFechaSesion().toString() + " Archivo:" + vtaq.getUrlVT());
            VTaquigraficaDTO vtaqAsuntos = new VTaquigraficaDTO();
            getVersionesTaquigraficasByFecha(vtaq.getId().toString(), fecha, vtaqAsuntos);
            listVtaqAsuntos.add(vtaqAsuntos);
            updateVtaqAsuntos(vtaq.getId().toString(), vtaqAsuntos);
        });

        return listVtaqAsuntos;
    }

    //Se obtiene el documento de la coleccion bd_sga_version_taquigrafica mediante la fechaSesion
    private void getVersionesTaquigraficasByFecha(String idEls, String fecha, VTaquigraficaDTO newVtaq) {

        Optional<List<BDVersionTaquigraficaDomain>> vtaq = null;
        vtaq = _dbVersionTaquigraficaRepository.findAllByFechaSesion(fecha);
        System.out.println("vtaq: " + vtaq.get());
        if (vtaq.isPresent() && vtaq.get().size() > 0) {
            if (vtaq.get().size() >= 1) {
                getVersionesTaquigraficasByAsuntoTemas(vtaq.get().get(0).getId(), newVtaq);
            }
        } else {
            System.out.println("Este no contiene datos: " + idEls);
        }
    }

    //Se obtienen los Temas, Expediente de la Version Taq
    private void getVersionesTaquigraficasByAsuntoTemas(String idVtaquigrafica, VTaquigraficaDTO newVtaq) {

        Optional<List<BDVtaqAsuntoDomain>> vtaqAsunTema = null;
        vtaqAsunTema = _bdVtaqAsuntoRepository.findAllByIdVtaquigrafica(idVtaquigrafica);
        if (vtaqAsunTema.isPresent()) {
            List<AsuntosTemasDTO> listAsuntosTemas = new ArrayList<>();
            vtaqAsunTema.get().forEach(xx -> {
                AsuntosTemasDTO asunto = new AsuntosTemasDTO();
                asunto.setTemasProcesalesAbordados(xx.getTemasProcesales());
                asunto.setTemasFondoAbordados(xx.getTemasFondo());
                getAuntoExpediente(xx.getIdAsunto(), asunto);
                listAsuntosTemas.add(asunto);
            });
            newVtaq.setAsuntos(listAsuntosTemas);
        } else {
            System.out.println("Esta no existe idVtaquigrafica dentro de bd_sga_vtaq_asunto : " + idVtaquigrafica);
        }
    }

    //Se obtienen los Asuntos de la Version Taq
    private void getAuntoExpediente(String idAsunto, AsuntosTemasDTO asunto) {

        Optional<DBAsuntoAbordadoDomain> asuntoExp = null;
        asuntoExp = _bdAsuntoAbordadoRepository.findById(idAsunto);
        if (asuntoExp.isPresent()) {
            asunto.setAsuntoAbordado(asuntoExp.get().getAsuntoAbordado());
            asunto.setNumExpediente(asuntoExp.get().getNumeroExpediente());
        } else {
            System.out.println("Esta no existe idAsunto dentro de bd_sga_asunto_abordado : " + idAsunto);
        }
    }

    //Actualiza la Version Taquigrafica de Elastic con la informacion de su version de SGA Mongo
    private void updateVtaqAsuntos(String Id, VTaquigraficaDTO asuntos) {
        org.springframework.data.elasticsearch.core.document.Document document = org.springframework.data.elasticsearch.core.document.Document
                .create();
        if (asuntos.getAsuntos() != null) {
            ArrayList<Object> list = new ArrayList<>();
            asuntos.getAsuntos().forEach(xx -> {
                Map<String, Object> asunto = new HashMap<>();
                asunto.put("asuntoAbordado", xx.getAsuntoAbordado());
                asunto.put("numExpediente", xx.getNumExpediente());
                asunto.put("temasFondoAbordados", xx.getTemasFondoAbordados());
                asunto.put("temasProcesalesAbordados", xx.getTemasProcesalesAbordados());
                //System.out.println(asunto);
                list.add(asunto);
            });
            document.put("asuntos", list);
            UpdateQuery builder3 = UpdateQuery.builder(Id).withDocument(document).build();
            _elasticsearchTemplate.update(builder3, IndexCoordinates.of(indice));
            System.out.println("Se actualizo id: " + Id + " Contiene: " + asuntos.getAsuntos().size());
        } else {
            System.out.println("Revisar este id (no tuvo match con Mongo): " + Id);
            System.out.println(asuntos.toString());
        }

    }

    //Actuaizacion (desde la ultima fehcaSession de Elastic o desde una fecha inicial ingresada) de las Versiones Taquigraficas de Elastic con la informacion SGA Mongo
    public List<String> updateVTaquigraficasDesdeBitacora(String fechaSesionInicio) throws ParseException {
         if(fechaSesionInicio == null || fechaSesionInicio == "" ){
             fechaSesionInicio = getUltimaFechaSesionElastic();
         }
        List<String> listFechasVersionesActualizadas = getVetaqByBitacora(fechaSesionInicio);
        //.limit(1)
        listFechasVersionesActualizadas.stream().forEach(id -> {
            VTaquigraficaDTO vtaqAsuntos = new VTaquigraficaDTO();
            getVersionesTaquigraficasByFecha(id, vtaqAsuntos);
            String IdElasticVTaq = getIDVersionTaqElasticByFecha(vtaqAsuntos.getFechaSesion());
            if(IdElasticVTaq == ""){
                System.out.println("Mongo ID: " + id + "  Elastic ID: " + IdElasticVTaq + "  " + vtaqAsuntos.getFechaSesion() + " <----");
            }
            else {
                updateVtaqAsuntos(IdElasticVTaq, vtaqAsuntos);
                System.out.println(vtaqAsuntos.getFechaSesion() + " <----");
            }
        });

        return listFechasVersionesActualizadas;
    }

    //Se obtiene la ultima fechaSesion del Indice de Elastic
    public String getUltimaFechaSesionElastic() {

        AtomicReference<String> fehaSesion = new AtomicReference<>("");
        QueryBuilder queryBuilder = QueryBuilders.boolQuery()
                .must(QueryBuilders.matchQuery("organoJurisdiccional", "Pleno").operator(Operator.AND))
                .must(QueryBuilders.rangeQuery("anio").gte(2019));
        Pageable pageable = PageRequest.of(0, 1);
        Query searchQuery = new NativeSearchQueryBuilder()
                .withQuery(queryBuilder)
                .withPageable(pageable)
                .build();
        searchQuery.addSort(Sort.by(Sort.Direction.DESC, "fechaSesion"));
        SearchHits resultHits = _elasticsearchTemplate.search(searchQuery, VersionesTaquigraficasAsuntos.class, IndexCoordinates.of(indice));

        resultHits.getSearchHits().forEach(xx -> {
            SearchHit result = (SearchHit) xx;
            VersionesTaquigraficasAsuntos objVTaq = (VersionesTaquigraficasAsuntos) result.getContent();
            fehaSesion.set(objVTaq.getFechaSesion());
        });
        return fehaSesion.get();
    }

    //Se obtienen los registros (Ids de Versiones Taquigraficas) modificados de Bitacora, aquellos que sean => a la fechaSesion de Elastic
    public List<String> getVetaqByBitacora(String strFechaInicio) throws ParseException {
        strFechaInicio = strFechaInicio.split("/")[2] + "-" + strFechaInicio.split("/")[1] + "-" + strFechaInicio.split("/")[0];
        //System.out.println("strFechaInicio: " + strFechaInicio);
        List<String> listFechasVersionesActualizadas = new ArrayList<>();
        String campoFecha = "commitMetaData.commitDate";
        String campoVersion = "state.idVersionTaqui";
        SimpleDateFormat formato = new SimpleDateFormat("yyyy-MM-dd");
        Date fechaInicio = formato.parse(strFechaInicio);
        //Date fechaFin = formato.parse(strFechaFin);
        org.springframework.data.mongodb.core.query.Query query = new
                org.springframework.data.mongodb.core.query.Query(
                Criteria.where(campoVersion).exists(true).andOperator(
                        Criteria.where(campoFecha).gte(fechaInicio))
        );
        //.andOperator(Criteria.where(campoFecha).lt(fechaFin))
        List<Object> listBitacora = _mongoTemplate.find(query, Object.class, "bd_sga_snapshots");
        //Integer count = 0;
        for (int i = 0; i < listBitacora.size(); i++) {
            LinkedHashMap<String, ArrayList<String>> ls = (LinkedHashMap<String, ArrayList<String>>) listBitacora.get(i);
            List<List<String>> l = new ArrayList<List<String>>(Collections.singleton(ls.get("state")));
            LinkedHashMap<String, ArrayList<String>> st = (LinkedHashMap<String, ArrayList<String>>) l.get(0);
            Object id = st.get("idVersionTaqui");
            listFechasVersionesActualizadas.add(id.toString());
        }
        return listFechasVersionesActualizadas.stream().distinct().collect(Collectors.toList());
    }

    //Con la fechaSesion de la Version Taquigrafica (Mongo) se obtiene el Documento correspondiente del Indice de Elastic
    public String getIDVersionTaqElasticByFecha(String fechaSesion) {
        fechaSesion = fechaSesion.split("-")[2] + "/" + fechaSesion.split("-")[1] + "/" + fechaSesion.split("-")[0];
        AtomicReference<String> idVTaqElastic = new AtomicReference<>("");
        QueryBuilder queryBuilder = QueryBuilders.boolQuery()
                .must(QueryBuilders.matchQuery("organoJurisdiccional", "Pleno").operator(Operator.AND))
                .must(QueryBuilders.termQuery("fechaSesion", fechaSesion))
                ;
        Pageable pageable = PageRequest.of(0, 1);
        final int limit = 1;
        Query searchQuery = new NativeSearchQueryBuilder()
                .withQuery(queryBuilder)
                .withPageable(pageable)
                .build();
        //searchQuery.addSort(Sort.by(Sort.Direction.DESC, "fechaSesion"));
        SearchHits resultHits = _elasticsearchTemplate.search(searchQuery, VersionesTaquigraficasAsuntos.class, IndexCoordinates.of(indice));
        //System.out.println("---->" + resultHits);
        resultHits.getSearchHits().forEach(xx -> {
            SearchHit result = (SearchHit) xx;
            //System.out.println(result.getContent());
            VersionesTaquigraficasAsuntos objVTaq = (VersionesTaquigraficasAsuntos) result.getContent();
            //System.out.println(objVTaq.getFechaSesion());
            idVTaqElastic.set(objVTaq.getId());
        });
        return idVTaqElastic.get();
    }

    //Se obtienen todos los datos necesarios de las colecciones relacionadas con Versiones Taquigraficas
    private void getVersionesTaquigraficasByFecha(String Id, VTaquigraficaDTO newVtaq) {

        Optional<BDVersionTaquigraficaDomain> vtaq = null;
        vtaq = _dbVersionTaquigraficaRepository.findById(Id);
        if (vtaq.isPresent()) {
            //System.out.println("vtaq: " + vtaq.get());
            newVtaq.setFechaSesion(vtaq.get().getFechaSesion());
            getVersionesTaquigraficasByAsuntoTemas(vtaq.get().getId(), newVtaq);
        } else {
            System.out.println("Este no contiene datos: " + Id);
        }
    }

}
