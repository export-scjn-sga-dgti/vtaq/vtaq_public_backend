package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.domains.AnyMongoDomain;

import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.dto.AsuntosDTO;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;
import java.util.List;

@Document(collection = "vtaqSesionesPreview")
public class VTaqSessionesPreview {

    @Id
    private String id;

    private Date fhSesion;

    private List<AsuntosDTO> asuntos;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getFhSesion() {
        return fhSesion;
    }

    public void setFhSesion(Date fhSesion) {
        this.fhSesion = fhSesion;
    }

    public List<AsuntosDTO> getAsuntos() {
        return asuntos;
    }

    public void setAsuntos(List<AsuntosDTO> asuntos) {
        this.asuntos = asuntos;
    }

    @Override
    public String toString() {
        return "VTaqSessionesPreview{" +
                "id='" + id + '\'' +
                ", fhSesion=" + fhSesion +
                ", asuntos=" + asuntos +
                '}';
    }
}
