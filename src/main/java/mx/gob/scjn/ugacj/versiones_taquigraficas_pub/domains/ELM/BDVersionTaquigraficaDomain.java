package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.domains.ELM;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "bd_sga_version_taquigrafica")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BDVersionTaquigraficaDomain {

    @Id
    private String id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private String fechaSesion;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFechaSesion() {
        return fechaSesion;
    }

    public void setFechaSesion(String fechaSesion) {
        this.fechaSesion = fechaSesion;
    }

    @Override
    public String toString() {
        return "BDVersionTaquigraficaDomain{" +
                "id='" + id + '\'' +
                ", fechaSesion='" + fechaSesion + '\'' +
                '}';
    }
}
