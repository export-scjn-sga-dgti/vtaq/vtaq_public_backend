package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.domains.ELM;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.List;

@Document(collection = "bd_sga_vtaq_asunto")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BDVtaqAsuntoDomain {

    @Id
    private String id;

    private String idVtaquigrafica;

    private String idAsunto;

    private List<String> temasProcesales;

    private List<String> temasFondo;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getIdVtaquigrafica() {
        return idVtaquigrafica;
    }

    public void setIdVtaquigrafica(String idVtaquigrafica) {
        this.idVtaquigrafica = idVtaquigrafica;
    }

    public String getIdAsunto() {
        return idAsunto;
    }

    public void setIdAsunto(String idAsunto) {
        this.idAsunto = idAsunto;
    }

    public List<String> getTemasProcesales() {
        return temasProcesales;
    }

    public void setTemasProcesales(List<String> temasProcesales) {
        this.temasProcesales = temasProcesales;
    }

    public List<String> getTemasFondo() {
        return temasFondo;
    }

    public void setTemasFondo(List<String> temasFondo) {
        this.temasFondo = temasFondo;
    }

    @Override
    public String toString() {
        return "BDVtaqAsuntoDomain{" +
                "id='" + id + '\'' +
                ", idVtaquigrafica='" + idVtaquigrafica + '\'' +
                ", idAsunto='" + idAsunto + '\'' +
                ", temasProcesales=" + temasProcesales +
                ", temasFondo=" + temasFondo +
                '}';
    }
}
