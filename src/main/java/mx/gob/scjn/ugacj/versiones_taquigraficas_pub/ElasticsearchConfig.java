package mx.gob.scjn.ugacj.versiones_taquigraficas_pub;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestClientBuilder;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;

import java.net.URI;
import java.net.URISyntaxException;

@Configuration
public class ElasticsearchConfig extends AbstractElasticsearchConfiguration {

    @Bean(name = { "elasticsearchOperations", "elasticsearchTemplate" })
    public ElasticsearchRestTemplate elasticsearchTemplate() {
        return new ElasticsearchRestTemplate(elasticsearchClient());
    }

    @Override
    public RestHighLevelClient elasticsearchClient() {
        RestClientBuilder clientBuilder = getRestClientBuilder("http://172.16.214.21:9200")
                .setHttpClientConfigCallback(requestConfig ->
                        //requestConfig.setKeepAliveStrategy((response, context) -> 100*1024*1024));
                        requestConfig.setKeepAliveStrategy((response, context) -> 60*1000))
                ;

        return new RestHighLevelClient(clientBuilder);
    }

    public static RestClientBuilder getRestClientBuilder(String esUrl) {
        return RestClient.builder(createHttpHost(URI.create(esUrl)));
    }

    public static HttpHost createHttpHost(URI uri) {
        if (StringUtils.isEmpty(uri.getUserInfo())) {
            return HttpHost.create(uri.toString());
        }
        try {
            return HttpHost.create(new URI(uri.getScheme(), null, uri.getHost(), uri.getPort(), uri.getPath(),
                    uri.getQuery(), uri.getFragment()).toString());
        } catch (URISyntaxException ex) {
            throw new IllegalStateException(ex);
        }
    }

}
