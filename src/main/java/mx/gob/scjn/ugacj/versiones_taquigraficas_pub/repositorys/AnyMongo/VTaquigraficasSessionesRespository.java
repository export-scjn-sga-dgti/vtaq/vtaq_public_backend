package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.repositorys.AnyMongo;

import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.domains.AnyMongoDomain.VTaqSessionesPreview;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VTaquigraficasSessionesRespository extends MongoRepository<VTaqSessionesPreview, String> {

    public Page<VTaqSessionesPreview> findAllByOrderByFhSesionDesc(Pageable paging);

}
