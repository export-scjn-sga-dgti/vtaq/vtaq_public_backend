package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.services.ELServ;


import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.dto.AsuntosTemasDTO;
import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.dto.VersionesTaquigraficasAsuntos;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.Operator;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.ParsedStringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.jsoup.Jsoup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.elasticsearch.core.*;
import org.springframework.data.elasticsearch.core.mapping.IndexCoordinates;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicReference;
import java.util.stream.Collectors;

import static org.elasticsearch.index.query.QueryBuilders.boolQuery;
import static org.elasticsearch.index.query.QueryBuilders.termsQuery;

@Service
public class VersionesTaqAsuntosService {

    @Autowired
    private ElasticsearchRestTemplate _elasticsearchTemplate;

    @Value("${indice}")
    private String indice;

    public List<String> getTiposAsuntos(){

        AbstractAggregationBuilder<TermsAggregationBuilder> agBuilder = AggregationBuilders.terms("asuntos.asuntoAbordado.keyword").field("asuntos.asuntoAbordado.keyword").size(100);

        Query query = new NativeSearchQueryBuilder()
                .withPageable(CustomBlankPage.PAGE)
                .withQuery(QueryBuilders.matchAllQuery())
                .addAggregation(agBuilder).build();

        SearchHits<VersionesTaquigraficasAsuntos> hits = _elasticsearchTemplate.search(query, VersionesTaquigraficasAsuntos.class);
        Aggregations aggs = hits.getAggregations();
        ParsedStringTerms namesTerm = (ParsedStringTerms) aggs.get("asuntos.asuntoAbordado.keyword");
        List<String> keys = namesTerm.getBuckets()
                .stream()
                .map(b -> b.getKeyAsString())
                .collect(Collectors.toList());
        //System.out.println(keys);
        return keys;
    }

    public Page<VersionesTaquigraficasAsuntos> getVersionesTaquigraficasByPleno(Integer page, Integer size, String filtros) throws ParseException {
        Page<VersionesTaquigraficasAsuntos> PageVersionesTaqPages = null;
        Pageable pageable = PageRequest.of(page - 1, size);
        int anio = 2019;
        QueryBuilder qb;
        BoolQueryBuilder queryFiltros = null;
        //BoolQueryBuilder  queryBuilderFechaSesion = QueryBuilders.boolQuery();
        BoolQueryBuilder queryBuilderRangeFechaAnioInicio = boolQuery()
                .must(QueryBuilders.rangeQuery("anio").gte(anio)
                );
        BoolQueryBuilder queryBuilderPrincipalTipoOrgano = boolQuery()
                .must(QueryBuilders.matchQuery("organoJurisdiccional", "Pleno")
                        .operator(Operator.AND)
                )
                /*.must(queryBuilderRangeFecha)
                .must(queryBuilderNumExp)
                .must(queryBuilderFechaSesion)*/;
        queryBuilderPrincipalTipoOrgano.must(queryBuilderRangeFechaAnioInicio);
        //String [] terminoBusqueda = new String[1];
        if (filtros != null) {
            //getQueryFiltro();
            queryBuilderPrincipalTipoOrgano.must(getQueryFiltro(filtros, queryFiltros));
        }

        //System.out.println(queryBuilderPrincipalTipoOrgano.toString());
        Query searchQuery = new NativeSearchQueryBuilder()
                .withQuery(queryBuilderPrincipalTipoOrgano)
                .withPageable(pageable)
                //.withHighlightFields(new HighlightBuilder.Field("*").preTags("<strong><mark>").postTags("</strong></mark>")) //fragmentSize(120)
                .withHighlightFields(
                        new HighlightBuilder.Field("asuntos.temasFondoAbordados")
                                .preTags("<strong><mark class=\"highlight\">").postTags("</strong></mark>").numOfFragments(0),
                        new HighlightBuilder.Field("asuntos.temasProcesalesAbordados")
                                .preTags("<strong><mark class=\"highlight\">").postTags("</strong></mark>").numOfFragments(0)
                )
                .build();

        searchQuery.addSort(Sort.by(Sort.Direction.DESC, "fechaSesion"));
        SearchHits resultHits = _elasticsearchTemplate.search(searchQuery, VersionesTaquigraficasAsuntos.class, IndexCoordinates.of(indice));
        //System.out.println("Total Hists: " + resultHits.getTotalHits());
        getResultsConvertHighligh(resultHits);
        SearchPage<VersionesTaquigraficasAsuntos> paginado = SearchHitSupport.searchPageFor(resultHits, pageable);
        PageVersionesTaqPages = (Page) paginado;
        return PageVersionesTaqPages;
    }

    private SearchHits getResultsConvertHighligh(SearchHits resultHits) {

        String temasFondoHighligh = "asuntos.temasFondoAbordados";
        String temasProcesalesHighligh = "asuntos.temasProcesalesAbordados";
        AtomicInteger counter = new AtomicInteger(0);
        resultHits.forEach(hit -> {
            //Se obtiene cada resultado de Contenct
            SearchHit resultado = (SearchHit) hit;
            //Se obtiene el resultado proveniente de los querys en su formato entidad
            VersionesTaquigraficasAsuntos vtaq = (VersionesTaquigraficasAsuntos) resultado.getContent();
            //System.out.println(counter.getAndIncrement() + ".- " +vtaq.getId());
            //De cada resultado, se obtiene el sub elemento donde vienen los temas
            List<AsuntosTemasDTO> asuntos = vtaq.getAsuntos();
            List<AsuntosTemasDTO> listNewAsuntos = new ArrayList<>();
            // Se verifica que el campo Highlight de cada resultado contenga datos los cuales se validaran
            if (((SearchHit<?>) resultado).getHighlightField(temasFondoHighligh).stream().collect(Collectors.toList()).size() > 0) {
                generateNuevosAsuntosConHighligh(resultado, temasFondoHighligh, asuntos, listNewAsuntos);
            }
            if (((SearchHit<?>) resultado).getHighlightField(temasProcesalesHighligh).stream().collect(Collectors.toList()).size() > 0) {
                generateNuevosAsuntosConHighligh(resultado, temasProcesalesHighligh, asuntos, listNewAsuntos);
            }
            //Si se detectaron elementos en la nueva lista, se sustituyen
            if (listNewAsuntos.size() > 0) {
                vtaq.setAsuntos(listNewAsuntos);
            }
        });
        return resultHits;
    }

    private void generateNuevosAsuntosConHighligh(SearchHit resultado, String tipoHighligh, List<AsuntosTemasDTO> asuntos, List<AsuntosTemasDTO> listNewAsuntos) {
        //Se obtiene y recorre la lista de contenido del campo Highlight
        ((SearchHit<?>) resultado).getHighlightField(tipoHighligh).stream().collect(Collectors.toList()).forEach(htmlTema -> {
            //Se obtiene cada resultado del campo Highlight y se le realiza una limpieza de su contenido html (etiquetas html asignadas cuando se realizo el query)
            String tema = Jsoup.parse(htmlTema).text();
            asuntos.forEach(tem -> {
                List<String> listTemas = null;
                //Se obtiene la lista de los temas de acuerdo al parametro tipoHighligh
                listTemas = tipoHighligh == "asuntos.temasFondoAbordados" ? tem.getTemasFondoAbordados() : tem.getTemasProcesalesAbordados();

                //Se verifica si el parrafo contiene doble espacio en blanco y se re emplaza solo por un espacio
                listTemas = listTemas.stream().map(xx -> xx = xx.contains("  ") ? xx.replace("  ", " ") : xx
                ).collect(Collectors.toList());
                //Se filtra la lista de los temas obtenidos, para determinar si el tema obtenido de la lista de Highligh lo contiene
                List<String> listAsun = listTemas.stream().filter(as -> as.contains(tema.trim()) || as.equals(tema.trim()) ).collect(Collectors.toList());
                //System.out.println(listAsun.size());
                //Si el filtrado anterior contiene datos, se guarda en una lista provicional los numExpediente
                if (listAsun.size() > 0) {
                    //Se ubicara y generara una nueva lista de temas Fondo/Procesales
                    //la cual solo contendra los temas que se hallan obtenido en el campo highlightFields
                    Optional<AsuntosTemasDTO> asunt = listNewAsuntos.stream().filter(xx -> xx.getNumExpediente().equals(tem.getNumExpediente())).findFirst();
                    if (asunt.isPresent()) {
                        if (tipoHighligh == "asuntos.temasFondoAbordados") {
                            asunt.get().setTemasFondoAbordados(getListNuevosTemascConHighligh(asunt.get().getTemasFondoAbordados(), htmlTema));
                        } else {
                            asunt.get().setTemasProcesalesAbordados(getListNuevosTemascConHighligh(asunt.get().getTemasProcesalesAbordados(), htmlTema));
                        }
                    } else {
                        listNewAsuntos.add(getNuevoAsuntoByTemaConHighlighv(tem, tipoHighligh, htmlTema));
                    }
                }
            });
        });
    }

    private AsuntosTemasDTO getNuevoAsuntoByTemaConHighlighv(AsuntosTemasDTO tem, String tipoHighligh, String htmlTema) {
        AsuntosTemasDTO convertAsun = new AsuntosTemasDTO();
        convertAsun.setAsuntoAbordado(tem.getAsuntoAbordado());
        convertAsun.setNumExpediente(tem.getNumExpediente());

        if (tipoHighligh == "asuntos.temasFondoAbordados") {
            convertAsun.setTemasFondoAbordados(Collections.singletonList(htmlTema));
            convertAsun.setTemasProcesalesAbordados(tem.getTemasProcesalesAbordados());
        } else {
            convertAsun.setTemasProcesalesAbordados(Collections.singletonList(htmlTema));
            convertAsun.setTemasFondoAbordados(tem.getTemasFondoAbordados());
        }
        return convertAsun;
    }

    private List<String> getListNuevosTemascConHighligh(List<String> listTemasAnt, String htmlTema) {
        List<String> newTemas = new ArrayList<>();
        listTemasAnt.forEach(zz -> {
            newTemas.add(zz);
        });
        newTemas.add(htmlTema);
        return newTemas;
    }

    private BoolQueryBuilder getQueryFiltro(String filtros, BoolQueryBuilder query) throws ParseException {
        query = boolQuery();
        String subs[] = filtros.split(",");
        for (String sub : subs) {
            String sTempFiltros[] = sub.split(":");
            String filtro = sTempFiltros[0].trim();
            String filtroValue = sTempFiltros[1].trim();
               /* System.out.println(filtro);
                System.out.println(filtroValue);*/
            switch (filtro) {
                case "numExpediente":
                    filtroValue = filtroValue.replace("-", "/");
                    query = boolQuery()
                            .must(termsQuery("asuntos.numExpediente.keyword", filtroValue)
                            )
                    ;
                    break;
                case "temasFondo":
                case "temasProcesal":
                case "Ambos":
                    String filtroTema = "";
                    switch (filtro) {
                        case "temasFondo":
                            filtroTema = "asuntos.temasFondoAbordados";
                           /* query = boolQuery()
                                    .must(QueryBuilders.queryStringQuery(filtroValue).field(filtroTema).defaultOperator(Operator.AND)
                                    )
                            ;
                             query = boolQuery()
                                    .must(QueryBuilders.matchQuery(filtroTema, filtroValue)
                                            .operator(Operator.AND)
                                    )
                            ;
                            */
                            query = boolQuery()
                                    .must(QueryBuilders.queryStringQuery( filtroValue).defaultField(filtroTema).defaultOperator(Operator.AND)
                                    )
                            ;
                            break;
                        case "temasProcesal":
                            filtroTema = "asuntos.temasProcesalesAbordados";
                           /* query = boolQuery()
                                    .must(QueryBuilders.matchQuery(filtroTema, filtroValue)
                                            .operator(Operator.AND)
                                    )
                            ;*/
                          /*query = boolQuery()
                                    .must(QueryBuilders.matchPhraseQuery(filtroTema, filtroValue)
                                    )
                            ;*/
                            query = boolQuery()
                                    .must(QueryBuilders.queryStringQuery( filtroValue).defaultField(filtroTema).defaultOperator(Operator.AND)
                                    )
                            ;
                            break;
                        case "Ambos":
                           /* query = boolQuery()
                                    .should(QueryBuilders.queryStringQuery(filtroValue).field("asuntos.*").defaultOperator(Operator.AND)
                                    )
                            ;*/
                           /* query = boolQuery()
                                    .should(boolQuery().must(QueryBuilders.matchPhraseQuery("asuntos.temasFondoAbordados", filtroValue))
                                            .should(QueryBuilders.matchPhraseQuery("asuntos.temasProcesalesAbordados", filtroValue)));*/
                            query = boolQuery()
                                    .should(QueryBuilders.queryStringQuery( filtroValue).defaultField("asuntos.temasFondoAbordados").defaultOperator(Operator.AND))
                                            .should(QueryBuilders.queryStringQuery( filtroValue).defaultField("asuntos.temasProcesalesAbordados").defaultOperator(Operator.AND));
                               /* query = boolQuery()
                                        .should(boolQuery().must(QueryBuilders.matchQuery("asuntos.temasFondoAbordados", filtroValue))
                                                .must(QueryBuilders.matchQuery("asuntos.temasProcesalesAbordados", filtroValue)));*/
                            break;
                    }
                    break;
                case "fechaSesion":
                    if(filtroValue.contains("-")){
                        String fechaInicio = filtroValue.replace("-", "/");
                        String strFechaFin = getFechaFin(fechaInicio);
                        query = boolQuery()
                                .must(QueryBuilders.rangeQuery("fechaSesion").gte(fechaInicio).lte(strFechaFin)
                                );
                    }
                    else {
                        if(filtroValue.length() < 4){
                            //Filtros por meses
                            query = boolQuery()
                                    .must(termsQuery("mes.numero", filtroValue)
                                    )
                            ;
                        }
                        else {
                            Integer anio = Integer.parseInt(filtroValue.toString());
                            query = boolQuery()
                                    .must(QueryBuilders.rangeQuery("anio").gte(anio).lte(anio)
                                    );
                        }
                    }
                    break;
                default:
                    query = boolQuery();
                    break;
                case "tipoAsunto":

                    query = boolQuery()
                            .must(termsQuery("asuntos.asuntoAbordado.keyword", filtroValue)
                            )
                    ;
                    break;
            }
        }
        return query;
    }

    private String getFechaFin(String fechiaInicio) throws ParseException {

        String fechaFin = "";
        LocalDate now = LocalDate.of(Integer.parseInt(fechiaInicio.split("/")[2]), Integer.parseInt(fechiaInicio.split("/")[1]), 1);
        LocalDate lastDay = now.with(TemporalAdjusters.lastDayOfMonth());
        //System.out.println(lastDay + " <---");
        //System.out.println(lastDay.getMonthValue());
        String mes = lastDay.getMonthValue() < 10 ? String.valueOf("0" + lastDay.getMonthValue()) : String.valueOf(lastDay.getMonthValue());
        //System.out.println("Nuevo mes: " + mes );
        fechaFin = lastDay.getDayOfMonth() + "/" + mes + "/" + lastDay.getYear();
        //System.out.println(fechaFin);
        return fechaFin;
    }

    public Integer getVersionesTaquigraficasByPlenoCountAsuntosHighligh(String filtros) throws ParseException {

        int anio = 2019;
        QueryBuilder qb;
        BoolQueryBuilder queryFiltros = null;
        //BoolQueryBuilder  queryBuilderFechaSesion = QueryBuilders.boolQuery();
        BoolQueryBuilder queryBuilderRangeFechaAnioInicio = boolQuery()
                .must(QueryBuilders.rangeQuery("anio").gte(anio)
                );
        BoolQueryBuilder queryBuilderPrincipalTipoOrgano = boolQuery()
                .must(QueryBuilders.matchQuery("organoJurisdiccional", "Pleno")
                        .operator(Operator.AND)
                )
                /*.must(queryBuilderRangeFecha)
                .must(queryBuilderNumExp)
                .must(queryBuilderFechaSesion)*/;
        queryBuilderPrincipalTipoOrgano.must(queryBuilderRangeFechaAnioInicio);
        //String [] terminoBusqueda = new String[1];
        if (filtros != null) {
            //getQueryFiltro();
            queryBuilderPrincipalTipoOrgano.must(getQueryFiltro(filtros, queryFiltros));
        }

        //System.out.println(queryBuilderPrincipalTipoOrgano.toString());
        Query searchQuery = new NativeSearchQueryBuilder()
                .withQuery(queryBuilderPrincipalTipoOrgano)
                //.withHighlightFields(new HighlightBuilder.Field("*").preTags("<strong><mark>").postTags("</strong></mark>")) //fragmentSize(120)
                .withHighlightFields(
                        new HighlightBuilder.Field("asuntos.temasFondoAbordados")
                                .preTags("<strong><mark class=\"highlight\">").postTags("</strong></mark>").numOfFragments(0),
                        new HighlightBuilder.Field("asuntos.temasProcesalesAbordados")
                                .preTags("<strong><mark class=\"highlight\">").postTags("</strong></mark>").numOfFragments(0)
                )
                .build();

        searchQuery.addSort(Sort.by(Sort.Direction.DESC, "fechaSesion"));
        SearchHits resultHits = _elasticsearchTemplate.search(searchQuery, VersionesTaquigraficasAsuntos.class, IndexCoordinates.of(indice));
        //System.out.println("Total Hists: " + resultHits.getTotalHits());
        getResultsConvertHighligh(resultHits);

        AtomicReference<Integer> count = new AtomicReference<>(0);
        resultHits.forEach(hit -> {

            SearchHit resultado = (SearchHit) hit;
            VersionesTaquigraficasAsuntos vtaq = (VersionesTaquigraficasAsuntos) resultado.getContent();

           // System.out.println(vtaq.getAsuntos().size());

            count.updateAndGet(v -> v + vtaq.getAsuntos().size());

        });
        return count.get();
    }

}
