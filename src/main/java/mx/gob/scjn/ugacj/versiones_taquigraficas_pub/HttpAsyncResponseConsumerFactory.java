package mx.gob.scjn.ugacj.versiones_taquigraficas_pub;


import org.apache.http.HttpResponse;
import org.apache.http.nio.protocol.HttpAsyncResponseConsumer;
import org.elasticsearch.client.HeapBufferedAsyncResponseConsumer;

//@Configuration
public interface HttpAsyncResponseConsumerFactory {
    /**
     * Creates the default type of {@link HttpAsyncResponseConsumer}, based on heap buffering with a buffer limit of 100MB.
     */
   // HttpAsyncResponseConsumerFactory DEFAULT = new HeapBufferedResponseConsumerFactory(DEFAULT_BUFFER_LIMIT);

    HttpAsyncResponseConsumerFactory DEFAULT = new HeapBufferedResponseConsumerFactory(100);

    /**
     * Creates the {@link HttpAsyncResponseConsumer}, called once per request attempt.
     */
    HttpAsyncResponseConsumer<HttpResponse> createHttpAsyncResponseConsumer();

    /**
     * Default factory used to create instances of {@link HttpAsyncResponseConsumer}.
     * Creates one instance of {@link HeapBufferedAsyncResponseConsumer} for each request attempt, with a configurable
     * buffer limit which defaults to 100MB.
     */
    class HeapBufferedResponseConsumerFactory implements HttpAsyncResponseConsumerFactory {

        //default buffer limit is 100MB
        static final int DEFAULT_BUFFER_LIMIT = 100 * 1024 * 1024;

        private final int bufferLimit;

        public HeapBufferedResponseConsumerFactory(int bufferLimitBytes) {
            this.bufferLimit = bufferLimitBytes;
        }

        @Override
        public HttpAsyncResponseConsumer<HttpResponse> createHttpAsyncResponseConsumer() {
            return new HeapBufferedAsyncResponseConsumer(bufferLimit);
        }
    }
}
