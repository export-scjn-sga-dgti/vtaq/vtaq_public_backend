package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.controllers.ELMC;

import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.dto.VTaquigraficaDTO;
import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.services.ELServ.ConvertVTaquigraficasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

@RestController
@RequestMapping( path = "/vtaquigraficas-convert")
public class VTaquigraficasConvertController {

    @Autowired
    private ConvertVTaquigraficasService _convertVTaquigraficasService;

    @RequestMapping( method = RequestMethod.GET)
    @CrossOrigin()
    public List<VTaquigraficaDTO> getVersionesByPleno(@RequestParam(name = "page") Integer page){
       return  _convertVTaquigraficasService.coversionDatos(page);
    }

    @RequestMapping( method = RequestMethod.GET, path = "/getVersionesActualizadas")
    @CrossOrigin()
    public List<String>  getVersionesByPleno(@RequestParam(required = false, name = "fechaInicio") String fechaInicio) throws ParseException {
        return _convertVTaquigraficasService.updateVTaquigraficasDesdeBitacora(fechaInicio);
    }

    @RequestMapping( method = RequestMethod.GET, path = "/getUltimaFecha")
    @CrossOrigin()
    public String  getUltimaFecha() throws ParseException {
        return _convertVTaquigraficasService.getUltimaFechaSesionElastic();
    }

}
