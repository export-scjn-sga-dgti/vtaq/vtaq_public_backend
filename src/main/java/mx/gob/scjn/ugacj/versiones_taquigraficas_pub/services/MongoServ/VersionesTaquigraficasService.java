package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.services.MongoServ;

import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.domains.AnyMongoDomain.VersionesTaquigraficas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationOperation;
import org.springframework.data.mongodb.core.aggregation.TypedAggregation;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.stereotype.Service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

@Service
public class VersionesTaquigraficasService {

    @Autowired
    private MongoOperations mongoOperations;

    public List<Integer> getFechasSessiones() {
        List<Date> emailIds = mongoOperations.query(VersionesTaquigraficas.class).distinct("fechaSesion").as(Date.class).all();
        List<Integer> listAnios = new ArrayList<>();
        emailIds.forEach(xx ->{
                    DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
                    String today = formatter.format(xx);
                    listAnios.add(Integer.parseInt(today.split("/")[2].toString()));
                }
               );
        Collections.reverse(listAnios);
        return  listAnios.stream().distinct().collect(Collectors.toList());
    }

    public Page<VersionesTaquigraficas> getAggVersionesTaquigraficas(Pageable paging, String filtros) throws ParseException {

        List<VersionesTaquigraficas> vTaquigrafica = null;
        //Se agregan los campos que se requieren en la Agregacion de tipo "asuntos"
        List<AggregationOperation> list = new ArrayList<AggregationOperation>();
        list.add(Aggregation.unwind("asuntos"));

        //Se aplican los filtros
        if (filtros != null) {
            String subs[] = filtros.split(",");
            for (String sub : subs) {
                String sTempFiltros[] = sub.split(":");
                String filtro = sTempFiltros[0].trim();
                String filtroValores = sTempFiltros[1].trim();
               // System.out.println(filtro);
                switch (filtro) {
                    case "numExpediente":
                        filtroValores = filtroValores.replace("-", "/");
                        list.add(Aggregation.match(Criteria.where("asuntos." + filtro).is(filtroValores)));
                        break;
                    case "temasFondo":
                    case "temasProcesal":
                    case "Ambos":
                        switch (filtro){
                            case "temasFondo":
                                list.add(Aggregation.match(getBusquedaTemasFondoOProcesal("asuntos.temasFondoAbordados.temas", filtroValores)));
                                break;
                            case "temasProcesal":
                                list.add(Aggregation.match(getBusquedaTemasFondoOProcesal("asuntos.temasProcesalesAbordados.temas", filtroValores)));
                                break;
                            case "Ambos":
                                list.add(Aggregation.match(getBusquedaTemasAmbos("asuntos.temasFondoAbordados.temas","asuntos.temasProcesalesAbordados.temas", filtroValores)));
                                break;
                        }
                        break;
                    case "fechaSesion":
                        list.add(Aggregation.match(getFiltroFecha(filtro, filtroValores)));
                        break;
                }
            }
        }

        //list.add(Aggregation.group("id", "fechaSesion", "asuntos"));
        list.add(Aggregation.sort(Sort.Direction.DESC, "fechaSesion"));
        list.add(Aggregation.project("id", "fechaSesion", "asuntos"));
        //list.add((AggregationOperation) new HighlightBuilder().field("asuntos"));

        //realiza la peticion de la Agregacion para obtener el total de elementos.
        TypedAggregation<VersionesTaquigraficas> agg1 = Aggregation.newAggregation(VersionesTaquigraficas.class, list);
        long count = mongoOperations.aggregate(agg1, VersionesTaquigraficas.class, VersionesTaquigraficas.class).getMappedResults().size();

        //Se integra las propiedades para que realise la paginacion de los resultados y devolver segun page y size
        list.add(Aggregation.skip(paging.getPageNumber() * paging.getPageSize()));
        list.add(Aggregation.limit(paging.getPageSize()));
        //list.add(Aggregation.facet(new AggregationOperation));
        TypedAggregation<VersionesTaquigraficas> agg = Aggregation.newAggregation(VersionesTaquigraficas.class, list);
        System.out.println(agg.toString());
        vTaquigrafica = mongoOperations.aggregate(agg, VersionesTaquigraficas.class, VersionesTaquigraficas.class).getMappedResults();
        vTaquigrafica.listIterator().next().getAsuntos().getTemasFondoAbordados().forEach(xx-> System.out.println());
        return new PageImpl<>(vTaquigrafica, paging, count);
    }

    private Criteria getBusquedaTemasFondoOProcesal(String filtroTema, String value) {
        return Criteria.where(filtroTema).regex(value.toString(), "i");
    }

    private Criteria getBusquedaTemasAmbos(String temaFondo, String temaProcesal, String value) {
        Criteria criteria = new Criteria();
        criteria.orOperator(Criteria.where(temaFondo).regex(value.toString(), "i"),Criteria.where(temaProcesal).regex(value.toString(), "i"));
        return criteria;
    }

    private Criteria getFiltroFecha(String filtro, String value) throws ParseException {
        Date fechaInicio = getFormatFecha(value);
        String strFechaFin = getFechaFin(Integer.parseInt(value.split("-")[2].toString()), Integer.parseInt(value.split("-")[1].toString())) + "-" +
                value.split("-")[1] + "-" + value.split("-")[2];
        Date fechaFin = getFormatFecha(strFechaFin);
        return Criteria.where(filtro).gte(fechaInicio).lte(fechaFin);
    }

    private Date getFormatFecha(String fecha) throws ParseException {
        SimpleDateFormat parser = new SimpleDateFormat("dd-MM-yyyy");
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        String fecha_parte1 = formatter.format(parser.parse(fecha)) + "T05:00:00.000Z";
        String defaultTimezone = TimeZone.getDefault().getID();
        Date date = (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ")).parse(fecha_parte1.replaceAll("Z$", "+0000"));
        return date;
    }

    private Integer getFechaFin(Integer anyo, Integer mes) {
        Calendar fecha = Calendar.getInstance();
        fecha.set(anyo, mes, 0);
        return fecha.getActualMaximum(Calendar.DAY_OF_MONTH);
    }

}
