package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.domains.AnyMongoDomain;


import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;

import java.time.LocalDate;
import java.util.List;

@Document(collection = "vtaqAsuntosPreview")
public class VTaqAsuntosPrevDomain {

    @Id
    //@Field("_id")
    private String id;
    private String tipoAsunto;
    private String numExpediente;
    private List<String> acumulados;
    private Boolean isAcumulada;
    private Boolean isCa;
    private String estatusAsunto;
    private Boolean isInListaUniverso;
    private List<String> asignaciones;
    private List<String> editoresTodos;
    private List<String> rubrosTodos;
    private List<String> temasProcesales;
    private List<String> temasFondo;
    private Integer numRubrosPublicadosSPSJF;
    private Integer totalParticipacionesTodas;
    private Integer porcentajeAvance;
    private List<String> sesionesTodas;
    private List<String> sesiones;
    private LocalDate primeraSession;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTipoAsunto() {
        return tipoAsunto;
    }

    public void setTipoAsunto(String tipoAsunto) {
        this.tipoAsunto = tipoAsunto;
    }

    public String getNumExpediente() {
        return numExpediente;
    }

    public void setNumExpediente(String numExpediente) {
        this.numExpediente = numExpediente;
    }

    public List<String> getAcumulados() {
        return acumulados;
    }

    public void setAcumulados(List<String> acumulados) {
        this.acumulados = acumulados;
    }

    public Boolean getAcumulada() {
        return isAcumulada;
    }

    public void setAcumulada(Boolean acumulada) {
        isAcumulada = acumulada;
    }

    public Boolean getCa() {
        return isCa;
    }

    public void setCa(Boolean ca) {
        isCa = ca;
    }

    public String getEstatusAsunto() {
        return estatusAsunto;
    }

    public void setEstatusAsunto(String estatusAsunto) {
        this.estatusAsunto = estatusAsunto;
    }

    public Boolean getInListaUniverso() {
        return isInListaUniverso;
    }

    public void setInListaUniverso(Boolean inListaUniverso) {
        isInListaUniverso = inListaUniverso;
    }

    public List<String> getAsignaciones() {
        return asignaciones;
    }

    public void setAsignaciones(List<String> asignaciones) {
        this.asignaciones = asignaciones;
    }

    public List<String> getEditoresTodos() {
        return editoresTodos;
    }

    public void setEditoresTodos(List<String> editoresTodos) {
        this.editoresTodos = editoresTodos;
    }

    public List<String> getRubrosTodos() {
        return rubrosTodos;
    }

    public void setRubrosTodos(List<String> rubrosTodos) {
        this.rubrosTodos = rubrosTodos;
    }

    public List<String> getTemasProcesales() {
        return temasProcesales;
    }

    public void setTemasProcesales(List<String> temasProcesales) {
        this.temasProcesales = temasProcesales;
    }

    public List<String> getTemasFondo() {
        return temasFondo;
    }

    public void setTemasFondo(List<String> temasFondo) {
        this.temasFondo = temasFondo;
    }

    public Integer getNumRubrosPublicadosSPSJF() {
        return numRubrosPublicadosSPSJF;
    }

    public void setNumRubrosPublicadosSPSJF(Integer numRubrosPublicadosSPSJF) {
        this.numRubrosPublicadosSPSJF = numRubrosPublicadosSPSJF;
    }

    public Integer getTotalParticipacionesTodas() {
        return totalParticipacionesTodas;
    }

    public void setTotalParticipacionesTodas(Integer totalParticipacionesTodas) {
        this.totalParticipacionesTodas = totalParticipacionesTodas;
    }

    public Integer getPorcentajeAvance() {
        return porcentajeAvance;
    }

    public void setPorcentajeAvance(Integer porcentajeAvance) {
        this.porcentajeAvance = porcentajeAvance;
    }

    public List<String> getSesionesTodas() {
        return sesionesTodas;
    }

    public void setSesionesTodas(List<String> sesionesTodas) {
        this.sesionesTodas = sesionesTodas;
    }

    public List<String> getSesiones() {
        return sesiones;
    }

    public void setSesiones(List<String> sesiones) {
        this.sesiones = sesiones;
    }

    public LocalDate getPrimeraSession() {
        return primeraSession;
    }

    public void setPrimeraSession(LocalDate primeraSession) {
        this.primeraSession = primeraSession;
    }

    @Override
    public String toString() {
        return "VTaqAsuntosPrevDomain{" +
                "id='" + id + '\'' +
                ", tipoAsunto='" + tipoAsunto + '\'' +
                ", numExpediente='" + numExpediente + '\'' +
                ", acumulados=" + acumulados +
                ", isAcumulada=" + isAcumulada +
                ", isCa=" + isCa +
                ", estatusAsunto='" + estatusAsunto + '\'' +
                ", isInListaUniverso=" + isInListaUniverso +
                ", asignaciones=" + asignaciones +
                ", editoresTodos=" + editoresTodos +
                ", rubrosTodos=" + rubrosTodos +
                ", temasProcesales=" + temasProcesales +
                ", temasFondo=" + temasFondo +
                ", numRubrosPublicadosSPSJF=" + numRubrosPublicadosSPSJF +
                ", totalParticipacionesTodas=" + totalParticipacionesTodas +
                ", porcentajeAvance=" + porcentajeAvance +
                ", sesionesTodas=" + sesionesTodas +
                ", sesiones=" + sesiones +
                ", primeraSession=" + primeraSession +
                '}';
    }
}
