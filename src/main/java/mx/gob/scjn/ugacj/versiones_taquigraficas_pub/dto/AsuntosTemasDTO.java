package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.dto;

import java.util.List;

public class AsuntosTemasDTO {

    private String asuntoAbordado;

    private String numExpediente;

    private List<String> temasFondoAbordados;

    private List<String> temasProcesalesAbordados;

    public String getAsuntoAbordado() {
        return asuntoAbordado;
    }

    public void setAsuntoAbordado(String asuntoAbordado) {
        this.asuntoAbordado = asuntoAbordado;
    }

    public String getNumExpediente() {
        return numExpediente;
    }

    public void setNumExpediente(String numExpediente) {
        this.numExpediente = numExpediente;
    }

    public List<String> getTemasFondoAbordados() {
        return temasFondoAbordados;
    }

    public void setTemasFondoAbordados(List<String> temasFondoAbordados) {
        this.temasFondoAbordados = temasFondoAbordados;
    }

    public List<String> getTemasProcesalesAbordados() {
        return temasProcesalesAbordados;
    }

    public void setTemasProcesalesAbordados(List<String> temasProcesalesAbordados) {
        this.temasProcesalesAbordados = temasProcesalesAbordados;
    }

    @Override
    public String toString() {
        return "AsuntosTemasDTO{" +
                "asuntoAbordado='" + asuntoAbordado + '\'' +
                ", numExpediente='" + numExpediente + '\'' +
                ", temasFondoAbordados=" + temasFondoAbordados +
                ", temasProcesalesAbordados=" + temasProcesalesAbordados +
                '}';
    }
}
