package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.domains.ELM;

import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document(collection = "bd_sga_asunto_abordado")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class DBAsuntoAbordadoDomain {

    @Id
    private String id;

    private  String asuntoAbordado;

    private String numeroExpediente;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAsuntoAbordado() {
        return asuntoAbordado;
    }

    public void setAsuntoAbordado(String asuntoAbordado) {
        this.asuntoAbordado = asuntoAbordado;
    }

    public String getNumeroExpediente() {
        return numeroExpediente;
    }

    public void setNumeroExpediente(String numeroExpediente) {
        this.numeroExpediente = numeroExpediente;
    }

    @Override
    public String toString() {
        return "DBAsuntoAbordadoDomain{" +
                "id='" + id + '\'' +
                ", asuntoAbordado='" + asuntoAbordado + '\'' +
                ", numeroExpediente='" + numeroExpediente + '\'' +
                '}';
    }
}
