package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.dto;

import java.util.List;

public class AsuntosDTO {

    private String asuntoAbordado;

    private String numExpediente;

    private List<TemasDTO> temasFondoAbordados;

    private List<TemasDTO> temasProcesalesAbordados;

    public String getAsuntoAbordado() {
        return asuntoAbordado;
    }

    public void setAsuntoAbordado(String asuntoAbordado) {
        this.asuntoAbordado = asuntoAbordado;
    }

    public String getNumExpediente() {
        return numExpediente;
    }

    public void setNumExpediente(String numExpediente) {
        this.numExpediente = numExpediente;
    }

    public List<TemasDTO> getTemasFondoAbordados() {
        return temasFondoAbordados;
    }

    public void setTemasFondoAbordados(List<TemasDTO> temasFondoAbordados) {
        this.temasFondoAbordados = temasFondoAbordados;
    }

    public List<TemasDTO> getTemasProcesalesAbordados() {
        return temasProcesalesAbordados;
    }

    public void setTemasProcesalesAbordados(List<TemasDTO> temasProcesalesAbordados) {
        this.temasProcesalesAbordados = temasProcesalesAbordados;
    }

    @Override
    public String toString() {
        return "AsuntosDTO{" +
                "asuntoAbordado='" + asuntoAbordado + '\'' +
                ", numExpediente='" + numExpediente + '\'' +
                ", temasFondoAbordados=" + temasFondoAbordados +
                ", temasProcesalesAbordados=" + temasProcesalesAbordados +
                '}';
    }
}
