package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.repositorys.ELMR;

import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.domains.ELM.BDVersionTaquigraficaDomain;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface BDVersionTaquigraficaRepository extends MongoRepository<BDVersionTaquigraficaDomain, String> {

    Optional<List<BDVersionTaquigraficaDomain>> findAllByFechaSesion(String fechaSesion);
}
