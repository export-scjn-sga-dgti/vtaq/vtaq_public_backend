package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.domains.AnyMongoDomain;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.dto.AsuntosDTO;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Document(collection = "versionesTaquigraficas")
@JsonInclude(JsonInclude.Include.NON_NULL)
public class VersionesTaquigraficas {

    @Id
    private String id;

    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
    private Date fechaSesion;

    private AsuntosDTO asuntos;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Date getFechaSesion() {
        return fechaSesion;
    }

    public void setFechaSesion(Date fechaSesion) {
        this.fechaSesion = fechaSesion;
    }

    public AsuntosDTO getAsuntos() {
        return asuntos;
    }

    public void setAsuntos(AsuntosDTO asuntos) {
        this.asuntos = asuntos;
    }
}
