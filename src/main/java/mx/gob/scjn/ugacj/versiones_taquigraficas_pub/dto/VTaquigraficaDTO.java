package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.dto;


import java.util.List;


public class VTaquigraficaDTO {

    /*   @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd-MM-yyyy")
     */
    private String fechaSesion;

    private List<AsuntosTemasDTO> asuntos;

    public String getFechaSesion() {
        return fechaSesion;
    }

    public void setFechaSesion(String fechaSesion) {
        this.fechaSesion = fechaSesion;
    }

    public List<AsuntosTemasDTO> getAsuntos() {
        return
                asuntos;
    }

    public void setAsuntos(List<AsuntosTemasDTO> asuntos) {
        this.asuntos = asuntos;
    }

    @Override
    public String toString() {
        return "VTaquigraficaDTO{" +
                "fechaSesion='" + fechaSesion + '\'' +
                ", asuntos=" + asuntos +
                '}';
    }
}
