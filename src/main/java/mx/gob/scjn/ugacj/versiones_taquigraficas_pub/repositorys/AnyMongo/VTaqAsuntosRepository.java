package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.repositorys.AnyMongo;

import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.domains.AnyMongoDomain.VTaqAsuntosPrevDomain;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VTaqAsuntosRepository extends MongoRepository<VTaqAsuntosPrevDomain, String> {

}
