package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.controllers.MongoC;

import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.domains.AnyMongoDomain.VTaqSessionesPreview;
import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.services.MongoServ.VTaquigraficasSessionesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;

@RestController
@RequestMapping( path = "/vtaquigraficas-asuntos-mongo")
public class VTaquigraficasAsuntosController {

    @Autowired
    private VTaquigraficasSessionesService _vTaquigraficasSessionesService;

    @RequestMapping( method = RequestMethod.GET)
    @CrossOrigin()
    public Page<VTaqSessionesPreview> getVTaquigraficasPorAsuntos(@RequestParam(name = "page") Integer page,
                                                                  @RequestParam(name = "size") Integer size,
                                                                  @RequestParam(required = false, name = "filtros") String filtros) throws ParseException {
        Pageable paging = PageRequest.of(page-1, size);
        return _vTaquigraficasSessionesService.getAllVTaquigraficasPages(paging, filtros);
    }

}
