package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.repositorys.AnyMongo;

import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.domains.AnyMongoDomain.VersionesTaquigraficas;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.Collection;

@Repository
public interface VersionesTaquigraficasRepository extends MongoRepository<VersionesTaquigraficas, String> {

    //@Aggregation(pipeline = { "{ '$group': { '_id' : 'fechaSesion' } }" })
    Collection<VersionesTaquigraficas> findAllFechaSesionDistinctBy();

}
