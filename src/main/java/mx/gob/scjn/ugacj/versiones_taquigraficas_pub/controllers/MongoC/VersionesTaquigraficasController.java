package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.controllers.MongoC;

import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.domains.AnyMongoDomain.VersionesTaquigraficas;
import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.services.MongoServ.VersionesTaquigraficasService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import java.text.ParseException;
import java.util.List;

@RestController
@RequestMapping( path = "/versiones-taquigraficas-m")
@CrossOrigin()
public class VersionesTaquigraficasController {

    @Autowired
    private VersionesTaquigraficasService _versionesTaquigraficasService;

    @RequestMapping( method = RequestMethod.GET)
    @CrossOrigin()
    public Page<VersionesTaquigraficas> getVtaquigraficas(@RequestParam(name = "page") Integer page,
                                                          @RequestParam(name = "size") Integer size,
                                                          @RequestParam(required = false, name = "filtros") String filtros) throws ParseException {
        Pageable paging = PageRequest.of(page-1, size);
        return _versionesTaquigraficasService.getAggVersionesTaquigraficas(paging, filtros);
    }

    @RequestMapping( method = RequestMethod.GET, path = "/fechas-sesiones")
    @CrossOrigin()
    public List<Integer> getFechasSesiones()  {

        return _versionesTaquigraficasService.getFechasSessiones();
    }

}
