package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.controllers.MongoC;

import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.domains.AnyMongoDomain.VTaqAsuntosExportDomain;
import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.domains.AnyMongoDomain.VTaqAsuntosPrevDomain;
import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.repositorys.AnyMongo.VTaquigraficasAsuntosRepository;
import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.services.MongoServ.VTaqAsuntosService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping( value = "/vtaquigraficas-mongo")
public class RecuperacionController {

    @Autowired
    private VTaqAsuntosService _vTaqAsuntosService;

    @Autowired
    private VTaquigraficasAsuntosRepository _VTaquigraficasAsuntosRepository;

    @RequestMapping(method = RequestMethod.GET,path = "/asuntosAll")
    @CrossOrigin()
    public Map<LocalDate, List<VTaqAsuntosExportDomain>> getAsuntosAll(){
        return _vTaqAsuntosService.getAllAsuntos();
    }

    @RequestMapping(method = RequestMethod.GET,path = "/porAsuntoExpediente/asunto/{asunto}/expediente/{expediente}")
    @CrossOrigin()
    public VTaqAsuntosPrevDomain getAcotada(@PathVariable String asunto, @PathVariable String expediente){
        expediente = expediente.replace("-", "/");
        return _VTaquigraficasAsuntosRepository.findByTipoAsuntoAndNumExpediente(asunto, expediente);
    }

}
