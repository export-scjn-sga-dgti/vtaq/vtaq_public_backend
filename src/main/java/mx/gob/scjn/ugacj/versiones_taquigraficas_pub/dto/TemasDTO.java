package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.dto;

import java.util.List;

public class TemasDTO {

    private List<String> temas;

    public List<String> getTemas() {
        return temas;
    }

    public void setTemas(List<String> temas) {
        this.temas = temas;
    }

    @Override
    public String toString() {
        return "TemasFondoDTO{" +
                "temas=" + temas +
                '}';
    }
}
