package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.repositorys.ELMR;

import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.dto.VersionesTaquigraficasAsuntos;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface VersionesTaqAsuntosRepository extends ElasticsearchRepository<VersionesTaquigraficasAsuntos, String> {

}
