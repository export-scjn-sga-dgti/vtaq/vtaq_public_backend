package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.services.MongoServ;

import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.domains.AnyMongoDomain.VTaqSessionesPreview;
import mx.gob.scjn.ugacj.versiones_taquigraficas_pub.repositorys.AnyMongo.VTaquigraficasSessionesRespository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.text.ParseException;
import java.util.List;

@Service
public class VTaquigraficasSessionesService {

    @Autowired
    private VTaquigraficasSessionesRespository _vTaquigraficasSessionesRespository;

    public Page<VTaqSessionesPreview> getAllVTaquigraficasPages(Pageable paging, String filtros) throws ParseException {

        long count = 0;
        Page<VTaqSessionesPreview> vTaquigraficaPaginable = null;
        List<VTaqSessionesPreview> vTaquigrafica = null;
        Page<VTaqSessionesPreview> vTaquigraficaPaginableResponse = null;
        Pageable paging2 = PageRequest.of(paging.getPageNumber() + 1, paging.getPageSize());
        /*if (filtros != null) {
            Query query = new Query().with(paging);
            Query queryNoPage = new Query();
            String subs[] = filtros.split(",");
            for (String sub : subs) {
                String sTempFiltros[] = sub.split(":");
                String filtro = sTempFiltros[0].trim();
                String filtroValores = sTempFiltros[1].trim();
                if (filtro.trim().equals("fechaResolucion")) {
                    Date fecha = ConvertFechaFiltros.getFormatFecha(filtroValores.trim());
                    query.addCriteria(Criteria.where(filtro).gte(fecha).lte(fecha));
                    queryNoPage.addCriteria(Criteria.where(filtro).gte(fecha).lte(fecha));
                } else {
                    query.addCriteria(Criteria.where(filtro).is(filtroValores));
                    queryNoPage.addCriteria(Criteria.where(filtro).is(filtroValores));
                }
            }
            List<Sentencia> listPrecedentes = mongoOperations.find(query, Sentencia.class);
            count = mongoOperations.count(queryNoPage, Sentencia.class);
            sentenciaPaginable = new PageImpl<Sentencia>(listPrecedentes, paging2, count);
            sentencias = sentenciaPaginable.getContent();
        } else {*/
        vTaquigraficaPaginable = _vTaquigraficasSessionesRespository.findAllByOrderByFhSesionDesc(paging);
        vTaquigrafica = vTaquigraficaPaginable.getContent();


        count = vTaquigraficaPaginable.getTotalElements();
        //}
        vTaquigraficaPaginableResponse = new PageImpl<VTaqSessionesPreview>(vTaquigrafica, paging2, count);
        return vTaquigraficaPaginableResponse;
    }





}
