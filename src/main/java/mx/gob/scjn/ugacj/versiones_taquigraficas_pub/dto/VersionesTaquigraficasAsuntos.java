package mx.gob.scjn.ugacj.versiones_taquigraficas_pub.dto;


import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.List;

@Document(indexName = "vtaquigraficas")
public class VersionesTaquigraficasAsuntos {

    @Id
    private String id;

    private String fechaSesion;

    private String video;

    private String urlVT;

    private List<AsuntosTemasDTO> asuntos;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFechaSesion() {
        return fechaSesion;
    }

    public void setFechaSesion(String fechaSesion) {
        this.fechaSesion = fechaSesion;
    }

    public String getVideo() {
        return video;
    }

    public void setVideo(String video) {
        this.video = video;
    }

    public String getUrlVT() {
        return urlVT;
    }

    public void setUrlVT(String urlVT) {
        this.urlVT = urlVT;
    }

    public List<AsuntosTemasDTO> getAsuntos() {
        return asuntos;
    }

    public void setAsuntos(List<AsuntosTemasDTO> asuntos) {
        this.asuntos = asuntos;
    }

    @Override
    public String toString() {
        return "VersionesTaquigraficasAsuntos{" +
                "id='" + id + '\'' +
                ", fechaSesion='" + fechaSesion + '\'' +
                ", video='" + video + '\'' +
                ", urlVT='" + urlVT + '\'' +
                ", asuntos=" + asuntos +
                '}';
    }
}
