package mx.gob.scjn.ugacj.versiones_taquigraficas_pub;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class VersionesTaquigraficasPubApplication extends SpringBootServletInitializer {

	public static void main(String[] args) {
		/*try {
			SSLContext ctx = SSLContext.getInstance("TLSv1.2");
			ctx.init(null, null, null);
			SSLContext.setDefault(ctx);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}*/
		SpringApplication.run(VersionesTaquigraficasPubApplication.class, args);
	}

}
